#!/usr/bin/env python3

# 189. Rotate Array (Easy)
# 
# Given an array, rotate the array to the right by k steps, where k is non-negative.
# 
# Follow up:
# Try to come up as many solutions as you can, there are at least 3 different ways to solve this problem.
# Could you do it in-place with O(1) extra space?

# Example
# Input: nums = [1,2,3,4,5,6,7], k = 3
# Output: [5,6,7,1,2,3,4]


# Note on my submission
# Runtime: 56 ms, faster than 95.04% of Python3 online submissions for Rotate Array.
# Memory Usage: 15.4 MB, less than 24.96% of Python3 online submissions for Rotate Array.

class Solution:
    def rotate(self, List, k):

        if k> len(nums):
            k = k%len(nums)

        # solution 1 accepted (O(1))
        nums[:k], nums[k:] = nums[-k:],  nums[:(len(nums)-k)]

        # solution 2, accepted (but O(n))
        #for i in range(k): nums.insert(0, nums.pop(-1))

nums = [1,2,3,4,5,6,7]; k = 3
nums = [-1,-100,3,99]; k = 2
nums = [1,2]; k = 3

print (nums)
sol = Solution()
sol.rotate(nums, k)
print (nums)
