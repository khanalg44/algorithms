#!/usr/bin/env python3

#   Leetcode Problem 48.

#   You are given an n x n 2D matrix representing an image.
#   Rotate the image by 90 degrees (clockwise).
#   Example:-----
#   Given input matrix = 
#   [ [1,2,3],
#     [4,5,6],
#     [7,8,9]]
#   
#   Rotate the input matrix in-place such that it becomes:
#   [ [7,4,1],
#     [8,5,2],
#     [9,6,3] ]
#-------------

class solution:
    def rotate(self, M):
        M.reverse()  # In place reversal of the matrix. rows are reversed.
        for i in range(len(M)):
            for j in range(i):
                # remember we have to switch the numbers at the indices i->j and j->i
                # and have to do it at the same time
                M[i][j], M[j][i]= M[j][i], M[i][j]

def PrintM(M):
    print ()
    for row in M:
        print (row)

if __name__=="__main__":
    M=[  [1,2,3], [4,5,6], [7,8,9] ]

    PrintM(M)
    sol=solution()
    sol.rotate(M)
    PrintM(M)

    M=[ [ 5, 1, 9,11], [ 2, 4, 8,10], [13, 3, 6, 7], [15,14,12,16] ]

    PrintM(M)
    sol=solution()
    sol.rotate(M)
    PrintM(M)

