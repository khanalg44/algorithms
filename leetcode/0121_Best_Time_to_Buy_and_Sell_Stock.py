#!/usr/bin/env python3


# 121 (easy)
# Say you have an array for which the ith element is the price of a given stock on day i.
# If you were only permitted to complete at most one transaction (i.e., buy one and sell
# one share of the stock), design an algorithm to find the maximum profit.
# Note that you cannot sell a stock before you buy one.

# ----- Examples -------
# Input: [7,1,5,3,6,4] :Output: 5 [buy 1 (pos=1) and sell 6 (pos=4): profit = 6-1 =5 ]
# Input: [7,6,4,3,1], Output: 0 [buy 1 (pos=4) and never sell, proit = 0]
# ----------------------

class Solution:
    def maxProfit(self, prices):
        min_val =  min(prices)
        min_indx = 0
        # first find the index of the min value
        # complexity O(n)
        for i in range(len(prices)):
            if prices[i]==min_val:
                min_indx = i
                break

        max_after_min = max(prices[min_indx:])
        
        return max_after_min - min_val

prices = [7,1,5,3,6,4]
prices = [7,6,4,3,1]
sol = Solution()
out = sol.maxProfit(prices)

print (out)
