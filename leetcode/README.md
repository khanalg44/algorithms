# LeetCodePractice

The problems can be found here
https://leetcode.com/

for some reason the ./gitignore is not working
so manually remove the z_res fie
git rm -rf ./z_res 
git commit -m 'removed z_res'
git push -u origin master

# Nice commands for one liner
* ``rstrip ``: strip from right. ``lstrip``:strip from left.
```python
    '001000'.rstrip('0') = '001'
    '001000'.lstrip('0') = '1000'
```

* check mutual elements in two lists
```
d1 = [0, 1, 2, 5]
d2 = [5, 3, 4]

arr = [i in d2 for i in d1]
l1 = any( arr )

print (arr, l1)
```
