#!/usr/bin/env python3

class Solution:
    def strStr(self, haystack, needle):
        
        if needle not in haystack:
            return -1
        elif needle == '':
            return 0
        else:
            for i in range(len(haystack)):
               if haystack[i:i+len(needle)] == needle:
                   return i

haystack = "hello"; needle = "ll"
haystack = "aaabaabb"; needle = "abb"
s = Solution()
print ( s.strStr(haystack, needle) )

