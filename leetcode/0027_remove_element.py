#!/usr/bin/env python3

# 27. Remove Element (Easy)
# Given an array nums and a value val, remove all instances of that value in-place and return the new length.
# Do not allocate extra space for another array, you must do this by modifying the input array in-place with O(1) extra memory.
# The order of elements can be changed. It doesn't matter what you leave beyond the new length.


class solution():
    
    def removeElement(self, nums, val):
        
        while val in nums:
            nums.remove(val)
        return len(nums)


nums = [3,2,2,3]
val = 3

s = solution()
print (s.removeElement(nums, val) )





