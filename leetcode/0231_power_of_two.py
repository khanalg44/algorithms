#!/usr/bin/env python3

# leetcode: 231 (easy)
# 
# Given an integer, write a function to determine if it is a power of two
# Examples: 
#     Input 1    : output True
#     Input 3    : output False
#     Input 218  : output False
#     Input 1024 : output True


class Solution:
    def isPowerOfTwo(self, n):
        #n=abs(n) # needed if n can be negative

        if (n==0):
            return False
        elif (n==1):
            return True

        # First convert the number to a binary where the first two character are 0b representing the binary number
        # The first actual number is always 1 so check if any of the rest of the digits are 1 return False if there are 1
        if '1' in bin(n)[3:]:  # first 3 digits are '0b1' always
            return False

        return True

    def isPowerOfTwo_2(self, n):
        return (bin(n)[2:].rstrip('0') ==1)


for i in range(1, 10000):
    sol = Solution()
    if sol.isPowerOfTwo_2(i):
        print (i, 'is power of 2')

n = 18
b = bin(n)

print (b[2:].rstrip('0'))
