#!/usr/bin/env python3

#35. Search Insert Position  (Easy)

# Given a sorted array and a target value, return the index if the target is found. 
# If not, return the index where it would be if it were inserted in order.
# You may assume no duplicates in the array.
# Example: Input: [1,3,5,6], 5; Output: 2
# Example: Input: [1,3,5,6], 2; Output: 1
# Example: Input: [1,3,5,6], 7; Output: 4


class Solution:

    def searchInsert(self, nums, target):
        for i, num in enumerate(nums):
            if num == target:
                print ('found equal at ', i)
                return i
            elif num > target:  # if in the middle of two numbers
                print ('found bigger at ', i)
                return i
        return len(nums)  # if in the end

    def searchInsert2(self, nums, target):
        for i, num in enumerate(nums):
            if num >= target:
                return i
        return len(nums)



nums = [1,3,5,6]; target= 5; #Output: 1
nums = [1,3,5,6]; target= 7; 
nums = [1,3,5,6]; target = 0
sol = Solution()
output = sol.searchInsert2(nums, target)

print (output)
