#!/usr/bin/env python3

# 15. Three sum (medium)

### Problem:
## Given an array of n integers, Find unique triplets of numbers [a, b, c] so that a+b+c=0].

#Example: 
# nums = [-1, 0, 1, 2, -1, -4]

# A solution set is:
# [ [-1, 0, 1],
# [-1, -1, 2]]


def ThreeSum(nums):
    Lst=[]
    for a in nums:
        for b in nums:
            for c in nums:
                #print ('sum:',a+b+c, Lst)
                if a+b+c==0 and [a, b, c].sort() not in Lst:
                    Lst.append([a, b, c].sort())
    #Lst=set(Lst)
    return Lst

def test():
    #a=[[1, 2], [2, 3], [1, 1], [1, 2]]
    #print (set(a))
    aa=[1, 2,4 , 5, -1, 0]
    aa.sort()
    print (aa)

def ThreeSum1(nums):
    return 0


if __name__=="__main__":
    nums=[-1, 1, 2, -1, -4]; print (ThreeSum(nums))
    #nums=[-1, 1, 2, -1, -4]; print (ThreeSum1(nums))
    #test()

